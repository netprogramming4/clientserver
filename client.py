import socket

HOST = '127.0.0.1'
PORT = 8080

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((HOST, PORT))

while True:
    data = client_socket.recv(1024).decode()
    if data.isdigit():  
        number = int(data)
        if number > 100:
            break

        print(f"Client received: {number}")

        number += 1
        client_socket.sendall(str(number).encode())
        print(f"Client sent: {number}")
    else:
        print("Invalid data received. Closing connection.")
        break

client_socket.close()
